FROM debian
RUN apt update && apt -y upgrade && apt -y install build-essential \
                                                   cmake \
                                                   git \
                                                   ssh \
                                                   gcc-arm-linux-gnueabihf g++-arm-linux-gnueabihf \
                                                   libssl-dev
